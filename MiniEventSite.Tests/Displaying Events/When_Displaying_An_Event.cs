﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using ApprovalTests;
using ApprovalTests.Reporters;
using MiniEventSite.Web.Controllers;
using MiniEventSite.Web.Models;
using MiniEventSite.Web.Models.Converters;
using MiniEventSite.Web.Models.DocumentTypes;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using NUnit.Framework;
using RazorGenerator.Testing;
using Umbraco.Core.Models;
using Umbraco.UnitTesting.Adapter.Support;
using Umbraco.Web;

namespace MiniEventSite.Tests.Displaying_Events
{
    [TestFixture]
    [UseReporter(typeof(VisualStudioReporter))]
    public class When_Displaying_An_Event
    {
        [Test]
        public void Then_Fetches_Details_From_EventSite()
        {
            var result = CallDisplayEvent();

            Assert.That(
                result.Model, 
                Is.InstanceOf<DisplayEventModel>()
                .And.Property("Name").EqualTo("Concert of your life")
            );
            Approvals.VerifyJson(JsonConvert.SerializeObject(result.Model));
        }

        [Test]
        [UseReporter(typeof(FileLauncherReporter), typeof(VisualStudioReporter))]
        public void Then_Shows_A_Linked_Name_And_The_Time()
        {
            var result = CallDisplayEvent();
            var view = new ASP._Views_Partials_DisplayEvent_cshtml();
            view.ViewContext = new ViewContext(controller.ControllerContext, Mock.Of<IView>(), new ViewDataDictionary(), new TempDataDictionary(), TextWriter.Null);

            var model = (DisplayEventModel)result.Model;
            var output = view.Render(model);

            Approvals.VerifyHtml(output);
        }

        private PartialViewResult CallDisplayEvent()
        {
            controller = new EventController(umbracoSupport.UmbracoContext, CreateHttpClient());
            umbracoSupport.PrepareController(controller);
            var result = controller.DisplayEvent().Result;
            return result;
        }

        private UmbracoSupport umbracoSupport;
        private EventController controller;

        [SetUp]
        public void Setup()
        {
            umbracoSupport = new UmbracoSupport();
            umbracoSupport.ContentCacheXml = @"
            <?xml version=""1.0"" ?>
            <!DOCTYPE root [
                <!ELEMENT home ANY>
                <!ATTLIST home id ID #REQUIRED>
                <!ELEMENT contentPage ANY>
                <!ATTLIST contentPage id ID #REQUIRED>
            ]>
            <root id=""-1"">
                <home id=""1103"" key=""156f1933-e327-4dce-b665-110d62720d03"" parentID=""-1"" level=""1"" creatorID=""0"" sortOrder=""0"" createDate=""2017-10-24T21:18:31"" updateDate=""2017-10-24T21:18:36"" nodeName=""Home"" urlName=""home"" path=""-1,1103"" isDoc="""" nodeType=""1093"" creatorName=""Admin"" writerName=""Admin"" writerID=""0"" template=""1064"" nodeTypeAlias=""home"">
                    <contentPage id=""1119"" key=""d62f0f1d-e4a9-4093-94ae-4efce18872ee"" parentID=""1103"" level=""2"" creatorID=""0"" sortOrder=""2"" createDate=""2017-10-24T21:18:31"" updateDate=""2017-10-24T21:18:36"" nodeName=""About Us"" urlName=""about-us"" path=""-1,1103,1119"" isDoc="""" nodeType=""1098"" creatorName=""Admin"" writerName=""Admin"" writerID=""0"" template=""1063"" nodeTypeAlias=""contentPage"">
                        <event>123</event>
                    </contentPage>
                </home>
            </root>
            ".Trim();

            umbracoSupport.ConverterTypes.Add(typeof(EventConverter));

            umbracoSupport.SetupUmbraco();

            umbracoSupport.SetupContentType("contentPage", new[]
            {
                new PropertyType("number", DataTypeDatabaseType.Integer, "event"), 
            });

            umbracoSupport.ModelFactory.Register("contentPage", c => new ContentPage(c));

            umbracoSupport.SetCurrentPage(GetContentPage());
        }

        private IPublishedContent GetContentPage()
        {
            return umbracoSupport.UmbracoContext.ContentCache.GetById(new Guid("d62f0f1d-e4a9-4093-94ae-4efce18872ee"));
        }

        [TearDown]
        public void Teardown()
        {
            umbracoSupport.DisposeUmbraco();
        }

        private HttpClient CreateHttpClient()
        {
            const string eventData = @"
            { 
                ""id"": 123,
                ""name"": ""Concert of your life"",
                ""time"": 2524607999
            }
            ";

            const string expectedUrl = "http://eventsite/event/123";

            var handler = Mock.Of<HttpMessageHandler>();

            Mock.Get(handler)
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync", 
                    ItExpr.Is<HttpRequestMessage>(m => m.RequestUri.AbsoluteUri == expectedUrl), 
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(new HttpResponseMessage
                {
                    Content = new StringContent(eventData, Encoding.Default, "application/json")
                });

            var client = new HttpClient(handler);
            return client;
        }
    }
}
