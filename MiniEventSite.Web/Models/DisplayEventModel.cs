﻿using System;

namespace MiniEventSite.Web.Models
{
    public class DisplayEventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Time { get; set; }
    }
}