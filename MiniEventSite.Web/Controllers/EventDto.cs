﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiniEventSite.Web.Controllers
{
    public class EventDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long Time { get; set; }
    }
}