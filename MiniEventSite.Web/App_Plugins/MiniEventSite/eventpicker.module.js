﻿(function() {

    angular.module("mini.event.site", []);

    try {
        angular.module("umbraco").requires.push("mini.event.site");
    } catch (e) {
        // Silently swallow when Umbraco's not there.
    }

}());