﻿(function() {

    var eventPickerDirective = {
        restrict: "E",
        templateUrl: "/App_Plugins/MiniEventSite/eventpicker.directive.html",
        replace: true,
        controller: "eventpicker.controller",
        scope: {
            model: "="
        }
    }

    angular.module("mini.event.site").directive("eventsPicker", function () {return eventPickerDirective});

}());