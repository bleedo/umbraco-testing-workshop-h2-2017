## Umbraco Unit Testing Workshop

This is a workshop I've done at the Umbraco UK Festival 2017 for now. 
The material is currently available for free under the MIT license.
Feel free to try and go through it yourself, but I can't promise to
be able to help out. I hope to repeat the workshop at another
festival or at CodeGarden, so do keep an eye out for a chance to
join a live workshop.

To follow along, you need to use the workshop-start branch.